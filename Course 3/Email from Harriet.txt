Email from Harriet Hadzic, Director of Data Analysis

Subject: RE: EDA & Data Viz

From: “Harriet Hadzic,” Harriet@waze 

Cc: ““May Santner,” May@waze; “Chidi Ga,” Chidi@waze 

Thanks for the update, Chidi!  

Welcome to the team. We’re so glad to have you. 

Along with the notebook, it would be really helpful if you included an executive summary of your analysis attached via email. 

I appreciate your help! 

Harriet Hadzic

Director of Data Analysis

Waze
