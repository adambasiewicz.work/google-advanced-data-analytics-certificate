Senior Data Analyst

Waze

- - - - - - - - - - - - - - - – - - - - – - - – - - -

Email from Harriet Hadzic, Director of Data Analysis 

Subject: New Request - Analyze rides based on device type

From: Harriet Hadzic,” Harriet@waze 

Cc: “May Santner,” May@waze; “Chidi Ga,” Chidi@waze; “Sylvester Esperanza,” Sylvester@Waze

I support this plan of action. Thank you all. 

Harriet Hadzic

Director of Data Analysis

Waze

- - - - - - - - - - - - - - - – - - - - – - - – - - -

Email from Chidi Ga, Senior Data Analyst 

Subject: New Request - Analyze rides based on device type

From: “Chidi Ga,” Chidi@waze 

Hi there, fellow data guru! 

You’ve been handling all of this work really well, by the way. Excellent job. 

I was wondering if you’d like to try the hypothesis test on the user data yourself? Based on what you’ve shared with me, I’m confident you have all the skills and experience needed for this task. 

What do you think? 

Also, as I said in my email to May, you’ll need to draft an executive summary of the results to share with Harriet and the rest of the leadership team. 

Thanks so much! 

Chidi Ga

Senior Data Analyst

Waze