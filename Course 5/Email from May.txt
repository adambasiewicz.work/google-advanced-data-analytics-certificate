Email from May Santner, Data Analysis Manager 

Subject: RE: Details on Regression Model

From: “May Santner,” May@waze

Cc: “Harriet Hadzic,” Harriet@waze; “Chidi Ga,” Chidi@waze; “Sylvester Esperanza,” Sylvester@Waze; “Ursula Sayo,” Ursula@waze

Thank you for your email. 

Apologies that the details were not made clear in our meeting.

To answer your question, we will build a binomial logistic regression model. Because we want to predict user churn, the binomial logistic regression model will be our confirmation for how best to proceed with the ML algorithm in the final phase of the project. 

Our team will be working on getting you the results of our analysis this week. 

Feel free to reach out with additional questions. 

Many thanks,

May Santner

Data Analysis Manager

Waze